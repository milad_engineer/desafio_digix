function getDataFromDatabase() {
    const dbState = {
        familias: [],
        status: []
    }

    const runGetDbDataFamilies = new Promise((resolve, reject) => {
        fetch("http://localhost:3001/familias")
            .then((response) => {
                if(response.ok) {
                    response.json().then((data) => {
                        console.log("> Inside get-database.js > Success in get familias...")
                        console.log("> Inside get-database.js > familias: ", dbState.familias)
                        dbState.familias = data
                        resolve()
                    })
                } else {
                    console.error("Erro during runGetDbDataFamilies() status != 200")
                    reject()
                }
            })
            .catch((error) => {
                console.error("> Inside get-database.js > Connection error in runGetDbDataFamilies - ERRO:", error)
                reject(error)
            })
    })

    const runGetDbDataStatus = new Promise((resolve, reject) => {
        fetch("http://localhost:3001/status")
            .then((response) => {
                if(response.ok) {
                    response.json().then((data) => {
                        console.log("> Inside get-database.js > Success in get status...")
                        console.log("> Inside get-database.js > status: ", dbState.status)
                        dbState.status = data
                        resolve()
                    })
                } else {
                    console.error("Erro during runGetDbDataStatus() status != 200")
                    reject()
                }
            })
            .catch((error) => {
                console.error("> Inside get-database.js > Connection error in runGetDbDataStatus - ERRO:", error)
                reject(error)
            })
    }) 

    function getDbStateFamilies(){
        return dbState.familias
    }

    function getDbStateStatus(){
        return dbState.status
    }

    return {
        runGetDbDataFamilies,
        runGetDbDataStatus,
        getDbStateFamilies,
        getDbStateStatus,
    }
}

module.exports = getDataFromDatabase