import toSelectFamilies from "./select-families.js"
import toSaveContemplate from "./save-contemplate.js"

export default function digixApp(){
    let contemplatedFamilies = [
        //{id: "", totalScore: 0, familyScoreSituation: {}, dateOfSelection: "", familyStatus: 0}
    ]

    function runSave(){
        if (contemplatedFamilies.length>0){
            contemplatedFamilies.forEach((contemplatedFamily) => {
                const instContemplatedFamilies = toSaveContemplate(contemplatedFamily)
                instContemplatedFamilies.saveContemplateList.then(() =>{ 
                    console.log("> Inside index.js > List of Contemplateds Saved with Success!")
                })
                .catch((error) =>{ 
                    console.error("> Inside index.js > List of Contemplateds Not Saved, Erro: ", error)
                })
            })
        } else {
            console.log("> Inside index.js > No data in Contemplateds List to Saved")
        }
    }

    async function runApp() {
        console.log("> Inside index.js > Running App!")
        const selectFamilies = toSelectFamilies()
        selectFamilies.runSelection.then(() => {
            contemplatedFamilies = selectFamilies.getContemplatedList()
            console.log("> Inside index.js > Saving Contemplateds List!: ")
            runSave()
        })
        .catch((error) => {
            console.error("> Inside index.js > Promisse returned an Error - ", error)
        })    
    }
    
    return {
        runApp,
    }
}