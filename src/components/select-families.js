import toGetDataFromDatabase from "./get-database.js"
import toGetFamiliesScore from "./score-families.js"

export default function selectFamilies(){
    const dbState = {
        familias: [],
        status: []
    }

    const preContemplateList = [
        //{id: "", totalScore: 0, familyScoreSituation: {}, dateOfPreSelection: "", familyStatus: 0}
    ]

    const contemplatedFamilies = [
        //{id: "", totalScore: 0, familyScoreSituation: {}, dateOfSelection: "", familyStatus: 0}
    ]

    const consultDatabase = new Promise((resolve, reject) => {
        const database  = toGetDataFromDatabase()
        console.log("> Inside select-families.js > Getting Data!")
        console.log("> Inside select-families.js > Promisse - Running Database!")
        database.runGetDbDataFamilies.then(() => {
            dbState.familias = database.getDbStateFamilies()
            console.log("> Inside select-families.js > Promisse Success - Familias: ", dbState.familias)
        })
        .catch((error) => console.error("> Inside select-families.js > Promisse runGetDbDataFamilies Catch - ", error))
        .finally(() => {
            database.runGetDbDataStatus.then(() => {
                dbState.status = database.getDbStateStatus()
                console.log("> Inside select-families.js > Promisse Success - Status: ", dbState.status)
            })
            .catch((error) => console.error("> Inside select-families.js > Promisse runGetDbDataStatus Catch - ", error))
            .finally(() => {
                if (dbState.familias && dbState.status){
                    console.log("> Inside select-families.js > Success in getting familias and state...")
                    console.log("> Inside select-families.js > dbState: ", dbState)
                    resolve()
                } else {
                    reject ("Erro during consultDatabase()")
                }
            })
        })
           
    })

    function scoreFamilies(family){
        const instScoreFamilies = toGetFamiliesScore(family)
        console.log("> Inside select-families.js > Getting Score Families!")
        let scoreFamily = instScoreFamilies.setScores()
        
        let totalFamilyScore = scoreFamily.totalScore
        delete scoreFamily.totalScore
        let today = new Date()
        
        preContemplateList.push({
            id: family.id, 
            totalScore: totalFamilyScore, 
            familyScoreSituation: scoreFamily, 
            dateOfPreSelection: today, 
            familyStatus: scoreFamily.familyStatus
        })
        console.log("> Inside select-families.js > Scoring Families! - Score: ", preContemplateList)
    }

    function selectContemplateds(){
        let today = new Date()
        preContemplateList.forEach((family, indexOfFamily) => {
            if (family.familyStatus == 0){
                contemplatedFamilies.push(family)
                delete contemplatedFamilies[contemplatedFamilies.length-1].dateOfPreSelection
                contemplatedFamilies[contemplatedFamilies.length-1].dateOfSelection = today
                preContemplateList.splice(indexOfFamily,1)
            }
        })
        console.log("> Inside select-families.js >Pre-Contemplateds: ", preContemplateList)
        console.log("> Inside select-families.js >Contemplateds: ", contemplatedFamilies)
    }

    const runSelection = new Promise((resolve, reject) => {
        consultDatabase.then(() => {
            dbState.familias.forEach(family => scoreFamilies(family))
        }).then(() => {
            selectContemplateds()
            resolve()
        }).catch((error) => {
            console.error("> Inside select-families.js > Promisse runSelection Catch - ", error)
            reject(error)
        })   
    })

    function getContemplatedList() {
        return contemplatedFamilies
    }
    
    return {
        runSelection,
        getContemplatedList,
    }
}