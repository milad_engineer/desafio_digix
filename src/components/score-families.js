function scoreFamilies(family){
    const objectFamily = family
    let familyScore = {
        financeUntil900: false,
        financeUntil1500: false,
        financeUntil2000: false,
        ageMoreThan45: false,
        ageMoreThan30to44: false,
        ageLessThan30: false,
        moreThan3Dependents: false,
        moreThan1to2Dependents: false,
        totalScore: 0,
        familyStatus: ""
    }

    function scoreFinanceSituation(){          
        var total = objectFamily.rendas.reduce((valAccumulator, currentValArray) => valAccumulator + currentValArray.valor, 0)
        console.log("> Inside score-families.js > Income: ", total)

        if (total>2000){
            familyScore.financeUntil900 = false
            familyScore.financeUntil1500 = false
            familyScore.financeUntil2000 = false
        } else if (total>=1501 && total<=2000){
            familyScore.financeUntil2000 = true
            familyScore.totalScore +=  1
        } else if (total>=901 && total<=1500){
            familyScore.financeUntil1500 = true
            familyScore.totalScore += 2
        } else { 
            familyScore.financeUntil900 = true
            familyScore.totalScore += 3
        }
    }

    function scoreAgeSituation(){
        let suitorBirth
        let suitorAge
        objectFamily.pessoas
        .filter((currentValArray) => currentValArray.tipo=="Pretendente")
        .forEach((valorArrayAtual) => suitorBirth =  valorArrayAtual.dataDeNascimento)

        let today = new Date()
        suitorBirth = new Date(suitorBirth)
        suitorAge = Math.floor(Math.ceil(Math.abs(suitorBirth.getTime() - today.getTime()) / (1000 * 3600 * 24)) / 365.25)
        console.log("> Inside score-families.js > Pretendente Age: ", suitorAge)
        
        if(suitorAge>=45){
            familyScore.ageMoreThan45 = true
            familyScore.totalScore += 3
        } else if (suitorAge>=30 && suitorAge>=44) {
            familyScore.ageMoreThan30to44 = true
            familyScore.totalScore += 2
        } else {
            familyScore.ageLessThan30 = true
            familyScore.totalScore += 1
        }
    }

    function scoreDependentsSituation(){
        let today = new Date().getTime();
        let arrayOfSuitorsAge = objectFamily.pessoas
        .filter((currentValArray) => currentValArray.tipo=="Dependente")
        .map((currentValArray) => {
            let suitorBirth = new Date(currentValArray.dataDeNascimento)
            let suitorAge = Math.floor(Math.ceil(Math.abs(suitorBirth.getTime() - today) / (1000 * 3600 * 24)) / 365.25)
            return suitorAge
        })
        .filter((currentValArray) => currentValArray<18)

        if (arrayOfSuitorsAge.length > 0 && arrayOfSuitorsAge.length <= 2) {
            familyScore.moreThan1to2Dependents = true
            familyScore.totalScore += 1
        } else {
            familyScore.moreThan3Dependents = true
            familyScore.totalScore += 2
        }
        familyScore.familyStatus = objectFamily.status 
        console.log("> Inside score-families.js > Dependents Situation: ", arrayOfSuitorsAge)
    }

    function setScores(){
        if (objectFamily.pessoas && objectFamily.rendas && objectFamily.status && objectFamily.id){
            scoreFinanceSituation()
            scoreAgeSituation()
            scoreDependentsSituation()
            console.log("> Inside score-families.js > Score: ", familyScore)
        } else {
            console.log("> Inside score-families.js > Warning: families data is undefined")
        }
        
        return familyScore
    }

    return {
        setScores
    }
}

module.exports = scoreFamilies