export default function saveContemplatedFamilies(contemplatedFamily){
    const saveContemplateList = new Promise((resolve, reject) => {
        console.log("> Inside save-contemplate.js > contemplatedFamily: ", contemplatedFamily)
        fetch("http://localhost:3001/contemplados/", { 
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(contemplatedFamily)
        })
        .then(function(response) { 
            let stringResponse = "> Inside save-contemplate.js > Status " +response.status + " - "
            
            if(response.status>=200 && response.status<300){
                stringResponse += "Data save with success!"
            } else if(response.status>=500) {
                stringResponse += "Internal server error (maybe this data already exists) - Erro data not saved!"
            } else {
                stringResponse += "Erro data not saved!"
            }
            response.ok ? console.log(stringResponse) : console.error(stringResponse)
        })
        .catch((error) => {
            reject(error)
        })
    })

    return {
        saveContemplateList
    }
}