const getDataFromDatabase = require("../components/get-database.js")
const scoreFamilies = require("../components/score-families.js")

module.exports = {
    getDataFromDatabase,
    scoreFamilies
}