const modules = require("../mock/mock-components.js").scoreFamilies

describe('Getting Score of Families', () => {
    let familyScore = {
        financeUntil900: expect.any(Boolean),
        financeUntil1500: expect.any(Boolean),
        financeUntil2000: expect.any(Boolean),
        ageMoreThan45: expect.any(Boolean),
        ageMoreThan30to44: expect.any(Boolean),
        ageLessThan30: expect.any(Boolean),
        moreThan3Dependents: expect.any(Boolean),
        moreThan1to2Dependents: expect.any(Boolean),
        totalScore: expect.any(Number),
        familyStatus: expect.any(String)
    }
    it('Should return no errors during running promisse setScores giving a null family',async () => {
        let fakeFamily = {}
        
        const instScoreFamilies = modules(fakeFamily)
        expect(instScoreFamilies.setScores()).toEqual(familyScore)
    })   
    it('Should return no errors during running promisse setScores giving a null family',async () => {
        let realFamily = {
        "id": "3dac7da3-d742-4e51-95f9-bbb37f522413",
        "pessoas": [
            {
            "id": "5e65eea1-aa72-407e-9a67-88045c07b5de",
            "nome": "João",
            "tipo": "Pretendente",
            "dataDeNascimento": "1989-12-30"
            },
            {
            "id": "d467781a-8f06-45ba-be6f-879cf32a9f7e",
            "nome": "Maria",
            "tipo": "Cônjuge",
            "dataDeNascimento": "1989-12-30"
            },
            {
            "id": "79820382-a181-42d2-bfae-6c012489e65e",
            "nome": "José",
            "tipo": "Dependente",
            "dataDeNascimento": "2015-12-30"
            },
            {
            "id": "80fa071e-17fb-4b87-99db-a7db0bfc23c2",
            "nome": "Angela",
            "tipo": "Dependente",
            "dataDeNascimento": "2012-12-30"
            }
        ],
        "rendas": [
            {
            "pessoaId": "5e65eea1-aa72-407e-9a67-88045c07b5de",
            "valor": 1000
            },
            {
            "pessoaId": "d467781a-8f06-45ba-be6f-879cf32a9f7e",
            "valor": 950
            }
        ],
        "status": "2"
        }
        const instScoreFamilies = modules(realFamily)
        expect(instScoreFamilies.setScores()).toEqual(familyScore)
    })
})    

