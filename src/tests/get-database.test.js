const modules = require("../mock/mock-components.js").getDataFromDatabase
const fetchMock = require( 'jest-fetch-mock')

fetchMock.enableMocks()
beforeEach(() => {
    fetch.resetMocks();
  });

describe('Getting Data Families From Database', () => {
    it('Should return no errors during running promisse runGetDbDataFamilies',async () => {
        fetch.mockResponseOnce(JSON.stringify({familias: []}))
        let instDatabase = modules()
        await expect(instDatabase.runGetDbDataFamilies).resolves
    })
    it('Should return an error during running promisse runGetDbDataFamilies',async () => {
        fetch.mockImplementationOnce(() => Promise.reject("API failure"))
        let instDatabase = modules()
        await expect(instDatabase.runGetDbDataFamilies).reject
    })
    it('Should return array of families data',async () => {
        fetch.mockResponseOnce(JSON.stringify({familias: []}))
        let instDatabase = modules()
        await instDatabase.runGetDbDataFamilies
        expect(instDatabase.getDbStateFamilies()).toEqual({"familias": expect.any(Array)})
    })
})

describe('Getting Data Status From Database', () => {
    it('Should return no errors during running promisse runGetDbDataStatus', async () => {
        fetch.mockResponseOnce(JSON.stringify({status: []}))
        let instDatabase = modules()
        await expect(instDatabase.runGetDbDataStatus).resolves
    })
    it('Should return an error during running promisse runGetDbDataStatus', async () => {
        fetch.mockImplementationOnce(() => Promise.reject("API failure"))
        let instDatabase = modules()
        await expect(instDatabase.runGetDbDataStatus).reject
    })
    it('Should return array of status data', async () => {
        fetch.mockResponseOnce(JSON.stringify({status: []}))
        let instDatabase = modules()
        instDatabase.runGetDbDataStatus
        expect(instDatabase.getDbStateStatus()).toEqual([])
    })
})

