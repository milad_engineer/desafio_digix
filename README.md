# Desafio Vaga Digix
- **Autor** Milad Roghanian
- **Data de Conclusão (v1.0)** 20/07/2020
- **Descrição** Teste da Digix para vaga de Desenvolvedor Front.
- Presente em: https://www.notion.so/Desafio-t-cnico-2f53347cf16a418fb75c67c978ab1a0e
- Link deste repositório: https://gitlab.com/milad_engineer/desafio_digix

# Necessário Para Executar
- node.js 11.14.0 
- npm 6.14.4

# Passos Para Executar

### Project setup
```
npm install
```

### Para executar o webpack e gerar o bundle.js em /dist
```
npm run build-dev
```

### Para colocar o servidor no ar na porta 3001
```
npm run server
```

### Para executar os testes
```
npm run test
```

### Para rodar todos os comandos anteriores na seguinte ordem: testes, webpack e server:3001
##### (caso de erro, executar com sudo ou mudar a porta)
```
npm run start
```
# Módulos

## get-database.js: Usa um servidor fake fetch javascript server para simular o acesso ao banco de dados, presente no arquivo db.json
### Função runGetDbDataFamilies: uma Promise que acessa o banco de dados pegando dados das famílias
### Função runGetDbDataStatus: uma Promise que acessa o banco de dados de status
### Função getDbStateFamilies: retorna os dados das famílias salvo localmente
### Função getDbStateStatus: retorna os dados dos status salvo localmente

## score-families.js: Recebe um objeto com os dados das famílias e calcula o seu score
### Função setScores: executa três funções internas: scoreFinanceSituation (para pontuar a situação funanceira), scoreAgeSituation (para pontuar a situação de idades), scoreDependentsSituation (para pontuar a situação de dependentes). Retorna um objeto contendo o resultado dessas informações e o score calculado.

## select-families.js: Acessa os dois módulos anteriores e executa cada etapa da seleção
### Função runSelection: Acessa outras funções internas que pegam os dados do banco, salvam, interam em cada objeto de família, pegam seu score, e salvam criando o vetor de pre-selecionados e o de selecionados com estes dados.
### Função getContemplatedList: retorna o vetor das famílias selecionadas. 

## digix-app.js: acessa o módulo de seleção, executando suas funções e salvando os selecionados no banco
### Função runApp: Acessa o módulo de seleção, executando e retornando o vetor de contemplados, salvando-o no banco. 



# Para Ver Execução
### Para verificar o resultado das execuções basta abrir o arquivo index.html no navegador e abrir o console para verificar os logs.


## Dúvidas (miladr100@gmail.com)
👊 Obrigado!